TESDA-CATIA RUBY ON RAILS TRAINING
----------------------------------
A free Ruby on Rails training sponsored by Technical Education 
and Skills Development Authority (TESDA) and CATIA Foundation, Inc.

http://www.catiafi.org/free-seminar-ruby-on-rails-web-development/

Resource Person:
Mr. Diwa L. Del Mundo
Director of Engineering
RubrikLabs Inc.

Dates:
April 4 – 5, 2013 (Thur. & Fri./8am – 12nn)
April 18 – 19, 2013 (Thur. & Fri./8am – 12nn)
April 25 – 26, 2013 (Thur. & Fri./8am – 12nn)

Venue: 
CADLab, Bldg. 7, TESDA Training Center Taguig Campus Enterprise, 
Bldg. 7, Taguig City

BLOG PROJECT
------------
The instructor uses a blog app as an example to demonstrate how to 
use Ruby on Rails. This blog project will be updated continuously 
as I learn more. Stay tuned.


--
Elinore Tenorio
e: elinore.tenorio@gmail.com
w: www.obitwaryo.com